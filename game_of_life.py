import matplotlib.pyplot as plt
import numpy as np
import random as rd
import matplotlib.animation as animation
import argparse

def generate_universe(x_lenght,y_lenght,dtype=int):
    #create a matrix of x_lenght reitarating for each row
    s = []
    for i in range(x_lenght):
        p = []
        for j in range(y_lenght):
            p.append(0)
        s.append(p)
    return s

seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "blinker": [[0,0,0],[1,1,1],[0,0,0],],
    "toad": [[0,0,0,0],[0,1,1,1],[1,1,1,0],[0,0,0,0],],
    "block":[[0,0,0,0],[0,1,1,0],[0,1,1,0],[0,0,0,0],],
    "snake": [[0,0,0,0],[1,0,1,1],[1,1,0,1],[0,0,0,0]],
    "ship": [[0,0,0,0],[0,1,1,0], [0,1,0,1],[0,0,1,1],[0,0,0,0],],
    "aircraft carrier": [[1,1,0,0],[1,0,0,1],[0,0,1,1],],
    "glider": [[0,1,0],[0,0,1],[1,1,1]],
    "light-weight spaceship": [[0,1,1,1,1],[1,0,0,0,1],[0,0,0,0,1],[1,0,0,1,0],],
    "long boat": [[0,1,0,0],[1,0,1,0],[0,1,0,1],[0,0,1,1],],
    "bee-hive":[[0,0,0,0,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,0,0,0,0],],
    "loaf":[[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,0,1,0],[0,0,0,1,0,0]],
    "tub": [[0,0,0,0,0],[0,0,1,0,0],[0,1,0,1,0],[0,0,1,0,0],[0,0,0,0,0],],
    "diehard": [[0,0,0,0,0,0,0,1,0], [0,1,1,0,0,0,0,0,0,0], [0,0,1,0,0,0,1,1,1,0]],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}

def create_seed(type_seed):
    seed = type_seed
    if type_seed == "r_pentomino":
        return [[0, 1, 1], [1, 1, 0], [0, 1, 0]]
    else:
        return type_seed
    
def add_seed_to_universe(seed,universe,x_start,y_start):
    #the user gets to choose the grid' dimensions
    for i in range(len(seed)):
    #we reiterate for each value of the seed list (row)                           
        for j in range(len(seed[0])):
        #we reiterate for each value of the seed list (column)                           
            universe[i+x_start][j+y_start]=seed[i][j]
            #we replace a part of the universe's grid by the seed for each corresponding value in the loop
    return universe

def display_universe(universe):
    plt.imshow(universe, cmap="grey")
    #we display the universe, and put color on the seed
    plt.show()
    return

def initialization():
    type_seed = input("choose your seed:")
    if type_seed in seeds:
        seed = seeds[type_seed]
    else:
        print("Your seed is wrong. Restart")
        type_seed = input("choose your seed:")
    #associate the variable seed to a particular type of seed in the dictionnary
    x_lenght = int(input("choose the x lenght of the grid:"))
    y_lenght = int(input("choose the y lenght of the grid:"))
    while x_lenght < len(seed[0]):
        #if the x lenght is inferior to the lenght of the seed, cannot work
        x_lenght = int(input(f"choose the lenght of your x axis bigger then {len(seed[0])}:"))
    while y_lenght < len(seed):
        #if the y lenght is inferior to the dimY of the seed, it will not work
        y_lenght = int(input(f"choose the lenght of your y axis bigger then {len(seed)}:"))
    universe = generate_universe(x_lenght,y_lenght)
    #we generate a universe with the dimensions that were chosen
    x_start = rd.randint (0, x_lenght-len(seed[0]))
    y_start = rd.randint (0, y_lenght-len(seed))
    #to put a seed randomly in the universe we pick an x_start randomly between 0 and the lenght of the universe - the lenght of the seed (check it's not over it); same for y
    universe = add_seed_to_universe(seed,universe,x_start,y_start)
    #we created the universe and then display it with the chosen seed
    return universe

def get_neighbors(matrix,x,y):
    neighbors = []
    
    #let's check each neighbors of the cell
    for i in range(x-1, x+2):
    # for each i around x (the dimX of our cell)
        for j in range(y-1, y+2):
        #for each j around y (the dimY of our cell)
            if (i != x or j != y):
                i = i % len(matrix) 
                j = j % len(matrix[0])
                neighbors.append(matrix[i][j])
    return neighbors

def survival(matrix, x, y):
    neighbor_states = get_neighbors(matrix, x, y)
    live_neighbor_count = sum(neighbor_states)
    #count the number of alive neighbors around the cell
 
    #if this cell is alive   
    if matrix[x][y]==1:
        if live_neighbor_count in (2,3):
        #it stays alive only if it has 2 or 3 alive neighbors
            return 1
        else:
        #else it dies
            return 0
    
    #if this cell is dead
    if matrix[x][y]==0:
        if live_neighbor_count==3:
        #it becomes alives if it has 3 neighbors
            return 1
        else:
        #else it stays dead
            return 0
        
#we create a new universe to not change the original one
def generation(universe):
    n_universe = generate_universe(len(universe), len(universe[0]))
    #we copy the universe and change it according to the survival of the cells
    for i in range(len(universe)):
        for j in range(len(universe[0])):
            if survival(universe, i, j) == 1:
                n_universe[i][j]=1
            else:
                n_universe[i][j]=0
    return n_universe

#we create a new universe for each iteration (num)
def game_life_simulate(universe, num):
    animation = []
    for i in range(num):
        universe = generation(universe)
    return animation

def animation1(universe, num):
    '''with a given universe and a number num (iterations) 
    you will have an animation of the changing universes by playing the game of life'''
    
    fig, ax = plt.subplots()
    img = ax.matshow(universe, cmap = 'binary', interpolation = 'nearest')
    def animate(num):
    # create a list of lists within one image which is a universe
        nonlocal universe
        universe = generation(universe)
        img.set_array(universe)
        return img,
        
    anim = animation.FuncAnimation(fig, animate,frames= num, interval=200, blit=False)
    plt.show()

def animation_user_input():
    '''let the user play the game by choosing the seed of their choice, 
    the size of the frame and the number of universe they want in the animation'''
    universe = initialization()
    num = input("number of frames?")
    animation1(universe, num)

def set_parameters(): 
    parser = argparse.ArgumentParser(description="Please to start the game, pick the number of rows and columns of the universe, the type of seed and its position in the universe (x and y), a type of cmap for the display, the number of iterations and the interval between them, and if you should save the file after playing ")
    parser.add_argument("--grid-x", help="enter the number of rows of the universe", default=50,type=int)
    parser.add_argument("--grid-y", help="enter the number of columns of the universe", default=50, type=int)
    parser.add_argument("--seed", help="enter a type of seed", default="r_pentomino", type=str)
    parser.add_argument("--seed-position-x", help="enter the row of the top left corner of where the seed should be implemented", default=20, type=int)
    parser.add_argument("--seed-position-y", help="enter the column of the top left corner of where the seed should be implemented", default=20, type=int)
    parser.add_argument("--cmap", help="enter a type of cmap", default="binary", type=str)
    parser.add_argument("--num", help="enter the number of universe iterations", default=30, type=int)
    parser.add_argument("--interval", help="enter the time interval between updates (milliseconds)", default=300, type=int)
    parser.add_argument("--save", help="enter if the animation should be saved", default=False, type=bool)

    args = parser.parse_args()
    
    grid_x = args.grid_x
    grid_y = args.grid_y
    seed_type = args.seed
    seed_position_x = args.seed_position_x
    seed_position_y = args.seed_position_y
    cmap = args.cmap
    num = args.num
    interval = args.interval
    save = args.save 
    
    return grid_x, grid_y, seed_type, seed_position_x,seed_position_y, cmap, num, interval,save
    
    

def animation2(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save):
    
    """
    :param  tuple (int, int) universe_size: dimensions of the universe
    :param seed: (list of lists, np.ndarray) initial starting array
    :param seed_position: (tuple (int, int)) coordinates where the top-left corner of the seed array should be pinned
    :param cmap: (str) the matplotlib cmap that should be used
    :param n_generations: (int) number of universe iterations, defaults to 30
    :param interval: (int) time interval between updates (milliseconds), defaults to 300ms
    :param save: (bool) whether the animation should be saved, defaults to False
    """
    
    seed = seeds[seed_type]
    universe = generate_universe(grid_x, grid_y)
    universe = add_seed_to_universe(seed, universe, seed_position_x, seed_position_y)
    fig, ax = plt.subplots()
    img = ax.matshow(universe, cmap = cmap, interpolation = 'nearest')
    def animate(num):
        nonlocal universe
        universe = generation(universe)
        img.set_array(universe)
        return img,
        
    anim = animation.FuncAnimation(fig, animate,frames= num, interval=interval, blit=True)
    if save == True:
        anim.save('basic_animation.mp4', fps = 30)
    plt.show()
    
if __name__ == '__main__':
    grid_x, grid_y, seed_type, seed_position_x,seed_position_y, cmap, num, interval,save= set_parameters()
    print(grid_x, grid_y, seed_type, seed_position_x,seed_position_y, cmap, num, interval,save)
    animation2(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)