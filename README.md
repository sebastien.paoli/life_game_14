# Life_game_14

## Getting Started 

1. Clone this repository to your local machine:

```
git clone https://gitlab-student.centralesupelec.fr/sebastien.paoli/life_game_14.git
```

2. Navigate to the cloned directory:
```
cd life_game_14
```

## Usage

To start the program you need to take the game_of_life.py file and display it on your computer.
Using the argparse module you can display the different inputs you need to have in order to start the game.

To do that:
- Open a new terminal and write the command below:

    ```
    python3 game_of_life.py -h 
    ```

This will open in your terminal a list of what you should put to start the game. You should have something like this:
```
    usage: game_of_life.py [-h] [--grid-x GRID_X] [--grid-y GRID_Y] [--seed SEED]
                       [--seed-position-x SEED_POSITION_X]
                       [--seed-position-y SEED_POSITION_Y] [--cmap CMAP] [--num NUM]
                       [--interval INTERVAL] [--save SAVE]

    Please to start the game, pick the number of rows and columns of the universe, the type
    of seed and its position in the universe (x and y), a type of cmap for the display, the
    number of iterations and the interval between them, and if you should save the file
    after playing

    options:
    -h, --help            show this help message and exit
    --grid-x GRID_X       enter the number of rows of the universe
    --grid-y GRID_Y       enter the number of columns of the universe
    --seed SEED           enter a type of seed
     --seed-position-x SEED_POSITION_X
                        enter the row of the top left corner of where the seed should
                        be implemented
    --seed-position-y SEED_POSITION_Y
                        enter the column of the top left corner of where the seed
                        should be implemented
     --cmap CMAP           enter a type of cmap
    --num NUM             enter the number of universe iterations
    --interval INTERVAL   enter the time interval between updates (milliseconds)
    --save SAVE           enter if the animation should be saved
```
Using this, you will now be able to pick your parameters and put them in your terminal. We already defined each parameters by default but you can edit each of them.  The default parameters are 50, 50, r_pentomino, 20, 20, binary, 30, 300, False. If you edit them, it should look like this if you wanna change every parameters: 
```
    python3 game_of_life.py --grid-x 60 --grid-y 60 --seed infinite  --seed-position-x 5 --seed-position-y 5 --cmap spring --num 50 --interval 500 --save True

```
Basically put the paramater like ```--param ```value with the value you picked with a space between each of them.

After starting your program, it should open a new window with the universe and the seed you picked and your game should start by itself.

You now have started your game of life.