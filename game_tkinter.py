from tkinter import *
from tkinter import PhotoImage
from game_of_life import *

window = Tk()

window.title("Conway's game of life")

screen_width = window.winfo_screenwidth()
screen_height = window.winfo_screenheight()

window.geometry(f"{screen_width}x{screen_height}")

frame = Frame(window)
frame.pack()

label_field = Label(frame, text="Please choose a seed", font=("Times New Roman", 16))
label_field.grid(row=0, column=0)

image_r_pentomino = PhotoImage(file="r_pentomino.png")
reduced_image1 = image_r_pentomino.subsample(2, 2)

def start_animation_r_pentomino():
    grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save = set_parameters()
    seed_type = "r_pentomino"  # Set seed_type to "r_pentomino"
    print(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)
    animation2(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)

button_r_pentomino = Button(frame, text='click', image=reduced_image1,command=start_animation_r_pentomino)
button_r_pentomino.grid(row=1, column=0)

image_loaf = PhotoImage(file="loaf.png")

def start_animation_loaf():
    grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save = set_parameters()
    seed_type = "loaf"  # Set seed_type to "loaf"
    print(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)
    animation2(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)

button_loaf = Button(frame, text='click', image=image_loaf,command=start_animation_loaf)
button_loaf.grid(row=1, column=2)

image_block = PhotoImage(file="block.png")

def start_animation_block():
    grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save = set_parameters()
    seed_type = "block"  # Set seed_type to "block"
    print(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)
    animation2(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)

button_block = Button(frame, text='click', image=image_block,command=start_animation_block)
button_block.grid(row=1, column=3)

image_bee_hive = PhotoImage(file="bee_hive.png")

def start_animation_bee_hive():
    grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save = set_parameters()
    seed_type = "bee_hive"  # Set seed_type to "bee_hive"
    print(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)
    animation2(grid_x, grid_y, seed_type, seed_position_x, seed_position_y, cmap, num, interval, save)

button_bee_hive = Button(frame, text='click', image=image_bee_hive,command=start_animation_bee_hive)
button_bee_hive.grid(row=1, column=4)

window.mainloop()
